import socket
import logging

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)
logger = logging.getLogger('client')


class Client:

    def __init__(self, port, host):
        self.port = port
        self.host = host

    def send(self, msg):
        sock = socket.create_connection((self.host, self.port))
        with sock:
            sock.sendall(msg.encode("utf8"))
            data = sock.recv(1024).decode('utf8')
            logger.info(f"Response from ({self.host}:{self.port}): {data}")
            return data
