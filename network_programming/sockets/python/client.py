import argparse
import logging
from cli_client import Client

logger = logging.getLogger('client')

parser = argparse.ArgumentParser(description="Simple Client Based TCP Server")
parser.add_argument('--host', help='Host to connect', default='0.0.0.0')
parser.add_argument('--port', help='Port to connect', default=8080)
parser.add_argument('--message', help='Message to send', default='Hello, world')
args = parser.parse_args()


if __name__ == '__main__':
    client = Client(host=args.host, port=args.port)
    client.send(args.message)
