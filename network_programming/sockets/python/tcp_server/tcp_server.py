import socket
import logging

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)
logger = logging.getLogger('tcpserver')


class TCPServer:

    def __init__(self, port=8080, host='0.0.0.0', handler=None):
        self.port = int(port)
        self.host = host
        self.handler = handler

    def start(self):
        logger.info(f"Starting server..")
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.bind((self.host, self.port))
            sock.listen()
            logger.info(f"Started successfully at {self.host}:{self.port}")
            self.__process_requests(sock, handler=self.handler)

    def __process_requests(self, sock=None, handler=None):
        try:
            while True:
                conn, addr = sock.accept()
                conn.settimeout(20)
                logger.warning(f"{addr} has connected")
                data = conn.recv(1024)
                with conn:
                    response = handler(data, addr)
                    conn.sendall(response.encode('utf8'))
        except Exception:
            sock.close()
