from tcp_server import TCPServer
import re
import argparse
import logging

logger = logging.getLogger('tcpserver')


parser = argparse.ArgumentParser(description="Simple Socket Based TCP Server")
parser.add_argument('--host', help='Host to listen', default='0.0.0.0')
parser.add_argument('--port', help='Port to accept connections', default=8080)

args = parser.parse_args()


def handle_connection(data, addr):
        # Как правило, по сети данные передаются в виде байтов.
        (client_ip, client_port) = addr
        decoded_data = data.decode('utf8')
        numbers = re.findall(r"\d+", decoded_data)
        logger.warning(f"Client: {client_ip}:{client_port} said: {decoded_data}")
        response = str(numbers) + '\n'
        return response


if __name__ == '__main__':
    server = TCPServer(port=args.port, host=args.host, handler=handle_connection)
    server.start()
