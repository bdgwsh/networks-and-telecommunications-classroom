package main

import (
	"bufio"
	"fmt"
	"net"
)

func handleConnection(conn net.Conn) {
	clientName := conn.RemoteAddr().String()

	fmt.Println("Connected user -- ", clientName)
	conn.Write([]byte("Welcome, " + clientName + "\n\r"))

	defer conn.Close()

	scanner := bufio.NewScanner(conn)

	for scanner.Scan() {
		text := scanner.Text()

		if text == "Exit" {
			conn.Write([]byte("Bye\n\r"))
			fmt.Println("User has disconnected")

		} else if text != "" {
			fmt.Println(clientName, " enters: ", text)
			conn.Write([]byte("You have entered " + text + "\n\r"))
		}

	}

}

func main() {
	listener, err := net.Listen("tcp", "localhost:8000")
	if err != nil {
		panic(err)
	}

	defer listener.Close()

	for {
		conn, err := listener.Accept()
		if err != nil {
			panic(err)
		}

		go handleConnection(conn)
	}
}
